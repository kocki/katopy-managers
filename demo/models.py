from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

from .managers import PublishManager


class BaseCustomModel(models.Model):
    creator = models.ForeignKey(User,)
    created = models.DateTimeField(auto_now_add=True,)
    expire = models.DateTimeField(null=True, blank=True, default=None,)
    is_published = models.BooleanField(default=True,)
    is_archived = models.BooleanField(default=False,)

    objects = PublishManager()
    all_objects = models.Manager()

    class Meta:
        abstract = True


class Article(BaseCustomModel):
    content = models.CharField(max_length=1000000,)

    def __str__(self):
        return self.content[:50]

    class Meta:
        ordering = ('-created',)
