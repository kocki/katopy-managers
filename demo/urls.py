from django.conf.urls import url

from .views import ArticleListView, CreatorListView

urlpatterns = [
    url(r'^$', ArticleListView.as_view(), name='article-list'),
    url(r'^by-creators/$', CreatorListView.as_view(), name='creator-list'),
]
