from __future__ import unicode_literals
from django.contrib import admin

from .models import Article


class UserFilteredModelAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return self.model.objects.all()


class ArticleAdmin(UserFilteredModelAdmin):
    list_display = (
        "id", "__str__", "creator", "created", "expire", "is_published",
        "is_archived",
    )


admin.site.register(Article, ArticleAdmin)
