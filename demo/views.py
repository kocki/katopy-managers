from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.views.generic.list import ListView

from .models import Article


class UserFilteredListView(ListView):

    def get_queryset(self, *args, **kwargs):
        return super(UserFilteredListView, self).get_queryset(
            *args, **kwargs).filter(creator=self.request.user)

    def get_context_data(self, *args, **kwargs):
        context = super(UserFilteredListView, self).get_context_data(
            *args, **kwargs)
        context.update({
            "current_user": self.request.user,
        })
        return context


class ArticleListView(UserFilteredListView):
    def get_queryset(self):
        return Article.objects.f()


class CreatorListView(ListView):
    def get_queryset(self):
        return User.objects.filter(
            article__isnull=False,
        ).distinct()

    def get_context_data(self, *args, **kwargs):
        context = super(UserFilteredListView, self).get_context_data(
            *args, **kwargs)
        context.update({
            "current_user": self.request.user,
        })
        return context
