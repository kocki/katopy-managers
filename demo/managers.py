from __future__ import unicode_literals

import datetime
from django.db import models
from django.db.models import Q
from threadlocals import threadlocals


class PublishManager(models.Manager):
    """
    Here we create logic which allow safely get data for public view.
    """
    use_for_related_fields = True

    def get_queryset(self):
        current_user = threadlocals.get_current_user()
        qs = super(PublishManager, self).get_queryset()
        if current_user and current_user.is_superuser:
            return qs
        elif current_user and current_user.is_authenticated():
            return qs.filter(creator=current_user)
        else:
            return qs.none()

    def f(self):

        current_user = threadlocals.get_current_user()
        current_time = datetime.datetime.now()

        if current_user and current_user.is_superuser:
            return self.get_queryset()
        elif current_user and current_user.is_authenticated():
            return self.get_queryset().filter(
                Q(expire__isnull=True)
                | Q(expire__gt=current_time),
                created__lte=current_time,
                is_published=True,
                is_archived=False,
            )
        else:
            return self.get_queryset().none()
